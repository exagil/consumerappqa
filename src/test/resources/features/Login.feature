Feature: Login

  Scenario: Successful Login
    Given I am on the login page
    When I login with "abhinandankothari@gmail.com" and "Password1"
    Then I should be on the home page
    And I click the help button
    And I click the GO-PAY button
    And I write minimum 30 character and click submit button
    Then I should see "Your request is now being processed, thank you for your feedback"
