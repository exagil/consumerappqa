Feature: GoRide

  Scenario: Go-Ride book a journey and pay via Go-pay
    Given I am on the login page
    When I login with "abhinandankothari@gmail.com" and "Password1"
    And tap on Go-Ride
    When On Go-Ride Page I enter From location "Oakwood Premier Cozmo Jakarta and location details 2333, 15th Floor"
    And On Go-Ride Page I enter To location as "Grand Kemang"
    And  On Go-Ride Page I verify the total price and place order with Go-Pay
    Then On Go-Ride Confirmation Page I cancel booking and select "I waited too long"