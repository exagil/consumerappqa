package pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoRidePage extends BasePage {
    @FindBy(id = "edit_text_source")
    WebElement editTextSource;

    @FindBy(id = "edit_text_destination")
    WebElement editTextDestination;

    @FindBy(id = "source_cb_layout")
    WebElement sourceButtonCancel;

    @FindBy(id = "dest_cb_layout")
    WebElement destinationButtonCancel;

    @FindBy(id = "discout_price_text_view")
    WebElement discoutPriceTextView;

    @FindBy(id = "book_now_layout")
    WebElement bookNowLayout;

    @FindBy(id = "gojek_credit_rd")
    WebElement gojekCreditRadioButton;

    public GoRidePage(AndroidDriver androidDriver) {
        super(androidDriver);
        PageFactory.initElements(androidDriver, this);
    }

    public void enterSourceLocation(String fromLocation) throws InterruptedException {
        waitForElementVisibility(editTextSource);
        enterLocation(editTextSource, fromLocation, sourceButtonCancel);
    }

    public void enterDestinationLocation(String toLocation) throws InterruptedException {
        waitForElementVisibility(editTextDestination);
        enterLocation(editTextDestination, toLocation, destinationButtonCancel);
    }

    private void enterLocation(WebElement editText, String toLocation, WebElement buttonCancel) throws InterruptedException {
        editText.click();
        buttonCancel.click();
        editText.sendKeys(toLocation);
        String firstLocationElement = toLocation.split("\\s+")[0].toLowerCase();
        String firstLocationElementXPath = "(//android.widget.TextView[contains(@text, '" + firstLocationElement + "')])[1]";
        waitForElementVisibilityByXPath(firstLocationElementXPath);
        androidDriver.findElement(By.xpath(firstLocationElementXPath)).click();
    }

    public void isGoPayPricePresent() {
        waitForElementVisibility(discoutPriceTextView);
        discoutPriceTextView.getText().contains("Rp. ");
    }

    public void orderWithGoPay() {
        waitForElementVisibility(gojekCreditRadioButton);
        gojekCreditRadioButton.click();
        bookNowLayout.click();
    }
}
