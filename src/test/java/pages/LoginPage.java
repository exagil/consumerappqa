package pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

public class LoginPage extends BasePage {
    private final AndroidDriver androidDriver;

    public LoginPage(AndroidDriver androidDriver) {
        super(androidDriver);
        this.androidDriver = androidDriver;
    }

    public void login(String email, String password) throws InterruptedException {
        waitForElementVisibilityById("input_email");
        androidDriver.findElement(By.id("input_email")).sendKeys(email);
        androidDriver.findElement(By.id("input_password")).sendKeys(password);
        androidDriver.findElement(By.id("button_signin")).click();
    }
}
