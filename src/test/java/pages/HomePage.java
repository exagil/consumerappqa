package pages;

import io.appium.java_client.android.AndroidDriver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {
    public static final int TIMEOUT_EIGHT_SECONDS = 8;
    private final AndroidDriver androidDriver;

    @FindBy(id = "text_service_name")
    public WebElement textServiceName;

    public HomePage(AndroidDriver androidDriver) {
        super(androidDriver);
        this.androidDriver = androidDriver;
        PageFactory.initElements(androidDriver, this);
    }

    public void isDisplayed() {
        waitForElementVisibilityById("text_gopay_balance");
        WebElement textGoPayBalance = androidDriver.findElement(By.id("text_gopay_balance"));
        Assert.assertTrue(textGoPayBalance.getText().contains("Rp. "));
    }

    public void clickOnHelp() {
        String xpathExpression = "//*[@text='Help']";
        waitForElementVisibilityByXPath(xpathExpression);
        androidDriver.findElement(By.xpath(xpathExpression)).click();
    }

    public void clickGoPay() {
        String xpathExpression = "//*[@text='Go-Pay']";
        waitForElementVisibilityByXPath(xpathExpression);
        androidDriver.findElement(By.xpath(xpathExpression)).click();
    }

    public void raiseGoPayHelpTicket() {
        androidDriver.findElement(By.id("etHelp")).sendKeys("qwertyuiopasdfghjklzxcvbnmqwertyuiop");
        androidDriver.findElement(By.id("btnSend")).click();
    }

    public void verifyResponse(String expectedString) {
        String xpathExpression = "//android.widget.TextView[@text=" + "'" + expectedString + "']";
        waitForElementVisibilityByXPath(xpathExpression);
        androidDriver.findElement(By.xpath(xpathExpression)).isDisplayed();
    }

    public void tapOnGoRide() throws InterruptedException {
        waitForElementVisibility(textServiceName);
        textServiceName.click();
    }
}
