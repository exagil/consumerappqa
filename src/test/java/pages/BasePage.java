package pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    private static final long TIMEOUT = 20;
    protected final AndroidDriver androidDriver;

    public BasePage(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    protected void waitForElementVisibility(WebElement webElement) {
        new WebDriverWait(androidDriver, TIMEOUT).until(ExpectedConditions.visibilityOf(webElement));
    }

    public void waitForElementVisibilityByXPath(String xPath) {
        new WebDriverWait(androidDriver, TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
    }

    public void waitForElementVisibilityById(String id) {
        new WebDriverWait(androidDriver, TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
    }
}
