package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.HomePage;
import pages.LoginPage;

import static steps.LifecycleSteps.androidDriver;

public class LoginSteps extends BaseSteps {
    private LoginPage loginPage = new LoginPage(androidDriver);

    @Given("^I am on the login page$")
    public void i_am_on_the_login_page() throws Throwable {
    }

    @When("^I login with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void i_login_with_and(String email, String password) throws Throwable {
        loginPage.login(email, password);
    }

    @Then("^I should be on the home page$")
    public void i_should_be_on_the_home_page() throws Throwable {
        new HomePage(androidDriver).isDisplayed();
    }

    @Given("^On Login Page I log in using details of goRideUser and tap on GO-RIDE$")
    public void onLoginPageILogInUsingDetailsOfGoRideUserAndTapOGoRide() throws Throwable {

    }
}
