package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import pages.HomePage;

public class HelpSteps extends BaseSteps {
    private HomePage homePage = new HomePage(LifecycleSteps.androidDriver);

    @And("^I click the help button$")
    public void iClickTheHelpButton() throws Throwable {
        homePage.clickOnHelp();
    }

    @And("^I click the GO-PAY button$")
    public void iClickTheGOPAYButton() throws Throwable {
        homePage.clickGoPay();
    }

    @And("^I write minimum (\\d+) character and click submit button$")
    public void iWriteMinimumCharacterAndClickSubmitButton(int arg0) throws Throwable {
        homePage.raiseGoPayHelpTicket();
    }

    @Then("^I should see \"([^\"]*)\"$")
    public void iShouldSee(String expectedString) throws Throwable {
        homePage.verifyResponse(expectedString);
    }
}
