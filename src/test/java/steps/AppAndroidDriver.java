package steps;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class AppAndroidDriver {
    private static AndroidDriver androidDriver;

    public static AndroidDriver getInstance() throws MalformedURLException {
        if (androidDriver == null) {
            File appFile = new File("app", "app-debug.apk");
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("deviceName", "Android");
            capabilities.setCapability("platformName", "Android");
            capabilities.setCapability("app", appFile.getAbsolutePath());
            androidDriver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
        }
        return androidDriver;
    }
}
