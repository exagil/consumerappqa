package steps;

import cucumber.api.java.en.And;
import pages.HomePage;

public class HomeSteps extends BaseSteps {
    private HomePage homePage = new HomePage(LifecycleSteps.androidDriver);

    @And("^tap on Go-Ride$")
    public void tapOnGoRide() throws Throwable {
        homePage.tapOnGoRide();
    }
}
