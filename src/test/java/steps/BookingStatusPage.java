package steps;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;

public class BookingStatusPage extends BasePage {
    @FindBy(id = "cancel_booking_button")
    WebElement cancelBookingButton;

    @FindBy(xpath = "//android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[3]/android.widget.Button[2]")
    WebElement buttonYes;

    @FindBy(id = "relativeRowItem")
    WebElement cancelReasonFirstListItem;

    @FindBy(id = "btnSubmit")
    WebElement buttonSubmit;

    public BookingStatusPage(AndroidDriver androidDriver) {
        super(androidDriver);
        PageFactory.initElements(androidDriver, this);
    }

    public void cancelBookingWithReason() {
        waitForElementVisibility(cancelBookingButton);
        cancelBookingButton.click();
        waitForElementVisibility(buttonYes);
        buttonYes.click();
        waitForElementVisibility(cancelReasonFirstListItem);
        cancelReasonFirstListItem.click();
        buttonSubmit.click();
    }
}
