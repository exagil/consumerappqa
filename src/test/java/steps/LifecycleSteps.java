package steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;

public class LifecycleSteps extends BaseSteps {
    public static AndroidDriver androidDriver;

    @Before
    public void setup() throws MalformedURLException {
        androidDriver = AppAndroidDriver.getInstance();
    }

    @After
    public void tearDown() {
        androidDriver.quit();
    }
}
