package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.GoRidePage;

public class GoRideSteps {
    GoRidePage goRidePage = new GoRidePage(LifecycleSteps.androidDriver);
    BookingStatusPage bookingStatusPage = new BookingStatusPage(LifecycleSteps.androidDriver);

    @When("^On Go-Ride Page I enter From location \"([^\"]*)\"$")
    public void onGoRidePageIEnterSourceLocation(String fromLocation) throws Throwable {
        goRidePage.enterSourceLocation(fromLocation);
    }

    @And("^On Go-Ride Page I enter To location as \"([^\"]*)\"")
    public void onGoRidePageIEnterDestinationLocation(String toLocation) throws Throwable {
        goRidePage.enterDestinationLocation(toLocation);
    }

    @And("^On Go-Ride Page I verify the total price and place order with Go-Pay$")
    public void onGoRidePageIVerifyTheTotalPriceAndPlaceOrderWithGoPay() throws Throwable {
        goRidePage.isGoPayPricePresent();
        goRidePage.orderWithGoPay();

    }

    @Then("^On Go-Ride Confirmation Page I cancel booking and select \"([^\"]*)\"")
    public void onGoRideConfirmationPageICancelBookingAndSelectIWaitedTooLong(String reason) throws Throwable {
        bookingStatusPage.cancelBookingWithReason();
    }
}
